
model_file=${PWD}/saved_models/dpr_biencoder.3 
outputs_dir=${PWD}/outputs/lic2022
test_file="${PWD}/data/test1.json" 

train_dense_encoder:
	CUDA_VISIBLE_DEVICES=1 python train_dense_encoder.py \
		+batch_size=16 \
		fp16=True \
		train_datasets="${PWD}/data/retriever/lic2022-train.json" \
		dev_datasets="${PWD}/data/retriever/lic2022-dev.json" \
		train=biencoder_local \
		output_dir="${outputs_dir}/checkpoints"

generate_dense_embeddings:
	CUDA_VISIBLE_DEVICES=0,1 python lic2022_generate_dense_embeddings.py \
		batch_size=128 \
		model_file=${model_file} \
		shard_id=0 \
		out_file=${outputs_dir}/dense_embeddings/faiss_passages_shard

dense_retrieval:
	CUDA_VISIBLE_DEVICES=0 python lic2022_dense_retriever.py \
		fp16=False \
		model_file=${model_file} \
		qa_dataset=${test_file} \
		index_path="${outputs_dir}/faiss_index" \
		ctx_datatsets=["${outputs_dir}/dense_embeddings"] \
		encoded_ctx_files=["${outputs_dir}/dense_embeddings/faiss_passages_shard_*"] \
		out_file=dense_retrieval_results.json
